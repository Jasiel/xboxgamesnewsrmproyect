﻿using System.Web;
using System.Web.Optimization;

namespace XboxGamesNewsRM
{
    public class BundleConfig
    {
        // Para obtener más información sobre las uniones, visite https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Home
            bundles.Add(new ScriptBundle("~/Home/js").Include(
                        "~/Scripts/core/jquery.min.js",
                        "~/Scripts/core/popper.min.js",
                        "~/Scripts/core/bootstrap-material-design.min.js",
                        "~/Scripts/plugins/moment.min.js",
                        "~/Scripts/plugins/bootstrap-datetimepicker",
                        "~/Scripts/plugins/nouislider.min.js",
                        "~/Scripts/material-kit.min.js",
                        //"~/Scripts/jquery.js",
                        //"~/Scripts/bootstrap.min.js",
                        //"~/Scripts/bootstrap.bundle.min.js",
                        "~/Scripts/noticias/noticias.js"));
            
            bundles.Add(new StyleBundle("~/Home/css").Include(
                      "~/Content/material-kit.css",
                      //"~/Content/bootstrap.min.css",
                      //"~/Content/bootstrap-grid.min.css",
                      //"~/Content/bootstrap-reboot.min.css",
                      "~/Content/fonts.css",
                      "~/Content/noticias/noticias.css"));
            #endregion
        }
    }
}
