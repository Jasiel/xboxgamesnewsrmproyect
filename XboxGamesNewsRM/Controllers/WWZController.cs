﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XboxGamesNewsRM.Models.WWZ;

namespace XboxGamesNewsRM.Controllers
{
    public class WWZController : Controller
    {
        // GET: WWZ
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Guias()
        {

            return View(new GuiasWWZ()
            {
                descripcion = "No se seleciono ninguna Guia"
            });
        }
        [HttpGet]
        public ActionResult Guias(int id) {

            return View(new GuiasWWZ() { id=id, descripcion="Se encontro guia"});
        }
    }
}