﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace XboxGamesNewsRM.Controllers
{
    public class NoticiasController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Acerca()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contacto()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}